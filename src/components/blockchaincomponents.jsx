import React from "react";
import {
  Card,
  CardHeader,
  Heading,
  CardBody,
  Text,
  CardFooter,
  Button,
  InputGroup,
  InputRightAddon,
  InputLeftAddon,
  Input,
} from "@chakra-ui/react";

const Peer1 = () => {
  return (
    <div className="flex flex-row">
      <div className="pl-5">
        <div className="text-5xl">Blockchain</div>
        <div className="text-3xl">Peer A</div>
        <Card width={850} height={650}>
          <CardBody className="space-y-4" bgColor="#d8f0df">
            <div className="text-black text-base">Block</div>
            <InputGroup size="lg">
              <InputLeftAddon children="#" />
              <Input placeholder="1" width={800} bgColor={"white"} />
            </InputGroup>

            <div className="text-black text-base">Nonce</div>
            <InputGroup size="lg">
              <Input placeholder="" width={800} bgColor={"white"} />
            </InputGroup>

            <div className="text-black text-base">Coinbase</div>
            <div className="flex flex-row">
              <InputGroup size="lg">
                <InputLeftAddon children="$" bgColor={"white"} />
                <Input placeholder="" width={360} bgColor={"white"} />
              </InputGroup>

              <InputGroup size="lg">
                <InputLeftAddon children="From" bgColor={"white"} />
                <Input placeholder="" width={340} bgColor={"white"} />
              </InputGroup>
            </div>

            <div className="text-black text-base">TX</div>
            <div className="text-black text-base">Prev</div>
            <Input placeholder="" width={800} bgColor={"white"} />

            <div className="text-black text-base">Hash</div>
            <Input placeholder="" width={800} bgColor={"white"} />

            <CardFooter>
              <Button width="100px" bgColor="blue" textColor="white">
                Mine
              </Button>
            </CardFooter>
          </CardBody>
        </Card>
      </div>
      <div className="pl-5 pt-20">
        <Card width={850} height={650}>
          <CardBody className="space-y-4" bgColor="#d8f0df">
            <div className="text-black text-base">Block</div>
            <InputGroup size="lg">
              <InputLeftAddon children="#" />
              <Input placeholder="2" width={800} bgColor={"white"} />
            </InputGroup>

            <div className="text-black text-base">Nonce</div>
            <InputGroup size="lg">
              <Input placeholder="" width={800} bgColor={"white"} />
            </InputGroup>

            <div className="text-black text-base">Coinbase</div>
            <div className="flex flex-row">
              <InputGroup size="lg">
                <InputLeftAddon children="$" bgColor={"white"} />
                <Input placeholder="" width={360} bgColor={"white"} />
              </InputGroup>

              <InputGroup size="lg">
                <InputLeftAddon children="From" bgColor={"white"} />
                <Input placeholder="" width={340} bgColor={"white"} />
              </InputGroup>
            </div>

            <div className="text-black text-base">TX</div>
            <div>
                <div className="flex flex-row">
                <InputGroup size="lg">
                    <InputLeftAddon children="$" bgColor={"white"} />
                    <Input placeholder="" width={230} bgColor={"white"} />
                </InputGroup>
                <InputGroup size="lg">
                    <InputLeftAddon children="From" bgColor={"white"} />
                    <Input placeholder="" width={200} bgColor={"white"} />
                </InputGroup>
                <InputGroup size="lg">
                    <InputLeftAddon children="->" bgColor={"white"} />
                    <Input placeholder="" width={200} bgColor={"white"} />
                </InputGroup>
                </div>
                <div className="flex flex-row">
                    <InputGroup size="lg">
                        <InputLeftAddon children="Seg:" bgColor={"white"} />
                        <Input placeholder="" width={280} bgColor={"white"} />
                    </InputGroup>
                    <InputGroup size="lg">
                        <InputLeftAddon children="Sig:" bgColor={"white"} />
                        <Input placeholder="" width={400} bgColor={"white"} />
                    </InputGroup>
                </div>
            </div>

            <div>
                <div className="flex flex-row">
                <InputGroup size="lg">
                    <InputLeftAddon children="$" bgColor={"white"} />
                    <Input placeholder="" width={230} bgColor={"white"} />
                </InputGroup>
                <InputGroup size="lg">
                    <InputLeftAddon children="From" bgColor={"white"} />
                    <Input placeholder="" width={200} bgColor={"white"} />
                </InputGroup>
                <InputGroup size="lg">
                    <InputLeftAddon children="->" bgColor={"white"} />
                    <Input placeholder="" width={200} bgColor={"white"} />
                </InputGroup>
                </div>
                <div className="flex flex-row">
                    <InputGroup size="lg">
                        <InputLeftAddon children="Seg:" bgColor={"white"} />
                        <Input placeholder="" width={280} bgColor={"white"} />
                    </InputGroup>
                    <InputGroup size="lg">
                        <InputLeftAddon children="Sig:" bgColor={"white"} />
                        <Input placeholder="" width={400} bgColor={"white"} />
                    </InputGroup>
                </div>
            </div>

            <div>
                <div className="flex flex-row">
                <InputGroup size="lg">
                    <InputLeftAddon children="$" bgColor={"white"} />
                    <Input placeholder="" width={230} bgColor={"white"} />
                </InputGroup>
                <InputGroup size="lg">
                    <InputLeftAddon children="From" bgColor={"white"} />
                    <Input placeholder="" width={200} bgColor={"white"} />
                </InputGroup>
                <InputGroup size="lg">
                    <InputLeftAddon children="->" bgColor={"white"} />
                    <Input placeholder="" width={200} bgColor={"white"} />
                </InputGroup>
                </div>
                <div className="flex flex-row">
                    <InputGroup size="lg">
                        <InputLeftAddon children="Seg:" bgColor={"white"} />
                        <Input placeholder="" width={280} bgColor={"white"} />
                    </InputGroup>
                    <InputGroup size="lg">
                        <InputLeftAddon children="Sig:" bgColor={"white"} />
                        <Input placeholder="" width={400} bgColor={"white"} />
                    </InputGroup>
                </div>
            </div>

            <div>
                <div className="flex flex-row">
                <InputGroup size="lg">
                    <InputLeftAddon children="$" bgColor={"white"} />
                    <Input placeholder="" width={230} bgColor={"white"} />
                </InputGroup>
                <InputGroup size="lg">
                    <InputLeftAddon children="From" bgColor={"white"} />
                    <Input placeholder="" width={200} bgColor={"white"} />
                </InputGroup>
                <InputGroup size="lg">
                    <InputLeftAddon children="->" bgColor={"white"} />
                    <Input placeholder="" width={200} bgColor={"white"} />
                </InputGroup>
                </div>
                <div className="flex flex-row">
                    <InputGroup size="lg">
                        <InputLeftAddon children="Seg:" bgColor={"white"} />
                        <Input placeholder="" width={280} bgColor={"white"} />
                    </InputGroup>
                    <InputGroup size="lg">
                        <InputLeftAddon children="Sig:" bgColor={"white"} />
                        <Input placeholder="" width={400} bgColor={"white"} />
                    </InputGroup>
                </div>
            </div>

            <div className="text-black text-base">Prev</div>
            <Input width={800} bgColor={"white"}/>

            <div className="text-black text-base">Hash</div>
            <Input width={800} bgColor={"white"}/>

            <CardFooter>
              <Button width="100px" bgColor="blue" textColor="white">
                Mine
              </Button>
            </CardFooter>
          </CardBody>
        </Card>
      </div>
    </div>
  );
};

const Peer2 = () => {
  return (
    <div>
        
        <div className="pl-5">
        <div className="text-3xl">Peer B</div>
        <Card width={850} height={650}>
            <CardBody className="space-y-4" bgColor="#d8f0df">
            <div className="text-black text-base">Block</div>
            <InputGroup size="lg">
                <InputLeftAddon children="#" />
                <Input placeholder="" width={340} bgColor={"white"} />
            </InputGroup>

            <div className="text-black text-base">Nonce</div>
            <InputGroup size="lg">
                <Input placeholder="" width={340} bgColor={"white"} />
            </InputGroup>

            <div className="text-black text-base">Coinbase</div>
            <div className="flex flex-row">
                <InputGroup size="lg">
                <InputLeftAddon children="$" bgColor={"white"} />
                <Input placeholder="" width={360} bgColor={"white"} />
                </InputGroup>

                <InputGroup size="lg">
                <InputLeftAddon children="From" bgColor={"white"} />
                <Input placeholder="" width={340} bgColor={"white"} />
                </InputGroup>
            </div>

            <div className="text-black text-base">TX</div>
            <div className="text-black text-base">Prev</div>
            <Input placeholder="" width={800} bgColor={"white"} />

            <div className="text-black text-base">Hash</div>
            <Input placeholder="" width={800} bgColor={"white"} />

            <CardFooter>
                <Button width="100px" bgColor="blue" textColor="white">
                Mine
                </Button>
            </CardFooter>
            </CardBody>
        </Card>
        </div>
    </div>
  );
};

const Peer3 = () => {
  return (
    <div className="pl-5">
      <div className="text-3xl">Peer C</div>
      <Card width={850} height={650}>
        <CardBody className="space-y-4" bgColor="#d8f0df">
          <div className="text-black text-base">Block</div>
          <InputGroup size="lg">
            <InputLeftAddon children="#" />
            <Input placeholder="" width={340} bgColor={"white"} />
          </InputGroup>

          <div className="text-black text-base">Nonce</div>
          <InputGroup size="lg">
            <Input placeholder="" width={340} bgColor={"white"} />
          </InputGroup>

          <div className="text-black text-base">Coinbase</div>
          <div className="flex flex-row">
            <InputGroup size="lg">
              <InputLeftAddon children="$" bgColor={"white"} />
              <Input placeholder="" width={360} bgColor={"white"} />
            </InputGroup>

            <InputGroup size="lg">
              <InputLeftAddon children="From" bgColor={"white"} />
              <Input placeholder="" width={340} bgColor={"white"} />
            </InputGroup>
          </div>

          <div className="text-black text-base">TX</div>
          <div className="text-black text-base">Prev</div>
          <Input placeholder="" width={800} bgColor={"white"} />

          <div className="text-black text-base">Hash</div>
          <Input placeholder="" width={800} bgColor={"white"} />

          <CardFooter>
            <Button width="100px" bgColor="blue" textColor="white">
              Mine
            </Button>
          </CardFooter>
        </CardBody>
      </Card>
    </div>
  );
};

const BlockchainComponent = () => {
  return (
    <div className="space-y-96">
      <Peer1 />
      <Peer2 />
      <Peer3 />
    </div>
  );
};

export default BlockchainComponent;
